import { Component } from "react";

class CountClick extends Component {
    constructor(props){
        super(props);
        this.state = {
            count: 0
        }
        //c1: Sử dụng bind trỏ this của function về class
        // this.clickChangeHangler = this.clickChangeHangler.bind(this);
    }
    //c1:Sử dụng bind trỏ this của function về class
    // clickChangeHangler() {
    //     this.setState({
    //         count: this.state.count + 1
    //     })
    // }

    // c2: dùng Error function 
    clickChangeHangler = () => {
        this.setState({
            count: this.state.count + 1
        })
    }
    render(){
        return (
            <div>
                <p>Count: {this.state.count}</p>
                <button onClick={this.clickChangeHangler}>Click here</button>
            </div>
        )
    }
}

export default CountClick;